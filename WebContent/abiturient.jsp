<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/mytaglib.tld" prefix="mytag"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="style.css" rel="stylesheet">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Форма для абитуриента</title>
</head>
<body>
	<h1>
		<strong>Уважаемый абитуриент заполни поля по порядку и
			старайся не допускать ошибок! Результаты появятся после регистрации
			вас администратором.</strong>
	</h1>
	<form name="form" action="AbiturInput" method="get"
		onsubmit="return validate()">
		<input type="hidden" name="command" value="forward" /> <br /> <br></br>
		Введи имя: <input type="text" name="name" value="" /> <br /> <br></br>
		Введи фамилию: <input type="text" name="last_name" value="" /><br />
		<br></br> Введи отчество: <input type="text" name="middle_name"
			value="" /><br /> <br></br> Введи день рождения в формате
		(2001-12-31): <input type="text" name="birthdate" value="" /><br /> <br></br>
		Введи день регистрации в формате (2001-12-31): <input type="text"
			name="datereg" value="" /><br /> <br></br>
		<p>
			<b>Какая у тебя средняя оценка в аттестате?:</b><Br> <input
				type="radio" name="markAttestat" value="3" />3<Br /> <input
				type="radio" name="markAttestat" value="3.5" />3.5<Br /> <input
				type="radio" name="markAttestat" value="4" />4<Br /> <input
				type="radio" name="markAttestat" value="4.5" />4.5<Br /> <input
				type="radio" name="markAttestat" value="5" />5<Br />
		</p>
		Введи название факультета: <input type="text" name="name_faculty"
			value="" /><br /> <br></br> Введи предмет 1: <input type="text"
			name="name_subject" value="" /><br />
		<p>
			<b>Какая у тебя оценка по этому предмету?:</b><Br> <input
				type="radio" name="mark" value="3" />3<Br /> <input type="radio"
				name="mark" value="4" />4<Br /> <input type="radio" name="mark"
				value="5" />5<Br />
		</p>
		Введи предмет 2: <input type="text" name="name_subject2" value="" /><br />
		<p>
			<b>Какая у тебя оценка по этому предмету?:</b><Br> <input
				type="radio" name="mark2" value="3" />3<Br /> <input type="radio"
				name="mark2" value="4" />4<Br /> <input type="radio" name="mark2"
				value="5" />5<Br />
		</p>
		<span style="color: red" id="nf"></span><br />
		<span style="color: red" id="nf2"></span><br />
		 <input type="submit"
			value="Yes" /><br />
	</form>
	<script type="text/javascript">
		function validate() {
			//Считаем значения из полей в переменные x и y
			var name = document.forms["form"]["name"].value;
			var last_name = document.forms["form"]["last_name"].value;
			var birthdate = document.forms["form"]["birthdate"].value;
			var middle_name = document.forms["form"]["middle_name"].value;
			var datereg = document.forms["form"]["datereg"].value;
			var markAttestat = document.forms["form"]["markAttestat"].value;
			var name_faculty = document.forms["form"]["name_faculty"].value;
			var name_subject = document.forms["form"]["name_subject"].value;
			var name_subject2 = document.forms["form"]["name_subject2"].value;
			var mark = document.forms["form"]["mark"].value;
			var mark2 = document.forms["form"]["mark2"].value;
			if (!form.birthdate.value.match(/[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01])/)
					|| !form.birthdate.value.match(/[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01])/)) {
				   document.getElementById("nf2").innerHTML="дата не соответствует формату";
				   return false;
			}
			if (!form.name.value.match(/^[а-яА-ЯёЁa-zA-Z]+$/)||!form.middle_name.value.match(/^[а-яА-ЯёЁa-zA-Z]+$/)
					||!form.last_name.value.match(/^[а-яА-ЯёЁa-zA-Z]+$/)||!form.name_subject2.value.match(/^[а-яА-ЯёЁa-zA-Z]+$/)
					||!form.name_faculty.value.match(/^[а-яА-ЯёЁa-zA-Z]+$/)||!form.name_subject.value.match(/^[а-яА-ЯёЁa-zA-Z]+$/)) {
				   document.getElementById("nf2").innerHTML=" поля название факультета, имя, отчество, фамилия, название предмета, должны содержать только буквы";
				   return false;
			}
			if (name.length == 0 || last_name.length == 0
					|| birthdate.length == 0 || middle_name.length == 0
					|| datereg.length == 0 || markAttestat.length == 0
					|| name_faculty.length == 0 || name_subject.length == 0
					|| name_subject2.length == 0 || mark.length == 0
					|| mark2.length == 0) {
				document.getElementById("nf").innerHTML = "все поля обязательны для заполнения.";
				return false;
			}
		}
	</script>

	<mytag:sqa num="1" name="${name}" last_name="${last_name}"
		middle_name="${middle_name}" birthdate="${birthdate}"
		datereg="${datereg}" markAttestat="${markAttestat}" mark="${mark}"
		name_subject="${name_subject}" name_faculty="${name_faculty}"
		mark2="${mark2}" name_subject2="${name_subject2}"></mytag:sqa>
	<h1>Хочешь посмотреть на результаты, заполни форму внизу</h1>
	
	<form name="form2" action="ResultOut" method="get" onsubmit="return validate2()">
		<input type="hidden" name="command" value="forward" /> <br /> Введи
		название факультета: <input type="text" name="name" value="" /><br />
		<span style="color: red" id="nf3"></span><br />
		 <input
			type="submit" value="Yes" /><br /> <span style="color: red" id="ng"></span><br />
		<br />
	</form>
	
	<script type="text/javascript">
		function validate2() {
			var name = document.forms["form2"]["name"].value;
			if (!form2.name.value.match(/^[а-яА-ЯёЁa-zA-Z]+$/)) {
				   document.getElementById("nf3").innerHTML="введите только буквы";
				   return false;
			}
		}
		</script>

	<mytag:sqa num="2" name="${list}" last_name="" middle_name=""
		birthdate="" datereg="" markAttestat="" mark="" name_subject=""
		name_faculty="" mark2="" name_subject2=""></mytag:sqa>

	<h1>
		<a href='index.jsp'>Назад</a>
	</h1>
</body>
</html>