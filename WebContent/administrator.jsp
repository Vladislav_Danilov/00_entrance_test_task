<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/mytaglib.tld" prefix="mytag"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="style.css" rel="stylesheet">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Форма для администратора</title>
</head>
<body>
	<h1>
		<strong>Уважаемый администратор заполните поля ведомости
			студентами прошедшими верификацию подлинности введенных данных. Для
			удобства внизу представленны все имеющиеся записи
			зарегестрировавшихся студентов.</strong>
	</h1>
	<form name="form" action="AbiturInputAdmin" method="get"
		onsubmit="return validate()">
		<input type="hidden" name="command" value="forward" /> <br /> <br></br>
		Введи ID формы оценок: <input type="text" name="idform" value="" /> <br />
		<br></br> Введи ID абитуриента: <input type="text" name="idabitur"
			value="" /><br /><br></br>Введите ID предмета: <input type="text"
			name="idsubject" value="" /><br /><br></br> Введите ID факультета:
		<input type="text" name="idfaculty" value="" /><br /> <br></br> <span
			style="color: red" id="nf"></span><br /> <input type="submit"
			value="Yes" /><br />
	</form>

	<script type="text/javascript">
		function validate() {
			//Считаем значения из полей в переменные
			var idform = document.forms["form"]["idform"].value;
			var idabitur = document.forms["form"]["idabitur"].value;
			var idsubject = document.forms["form"]["idsubject"].value;
			var idfaculty = document.forms["form"]["idfaculty"].value;
			//Если поле anything2 пустое выведем сообщение и предотвратим отправку формы
			if ((idform.length == 0) || (idabitur.lenght == 0)
					|| (idfaculty.lenght == 0)) {
				document.getElementById("nf").innerHTML = "все поля обязательны для заполнения";
				return false;
			}

			if ((!form.idform.value.match(/[0-9]/))
					|| (!form.idabitur.value.match(/[0-9]/))
					|| (!form.idsubject.value.match(/[0-9]/))
					|| (!form.idfaculty.value.match(/[0-9]/))) {
				document.getElementById("nf").innerHTML = "*введите цифры во все поля";
				return false;
			}

		}
	</script>

	<mytag:sqad num="1" idform="${idform}" idsubject="${idsubject}"
		idfaculty="${idfaculty}" idabitur="${idabitur}"></mytag:sqad>
	<h1>Полная информация о зарегестрировавшихся студентах.</h1>

	<mytag:sqad num="2" idform="" idsubject="" idfaculty="" idabitur=""></mytag:sqad>

	<h1>
		<a href='index.jsp'>Назад</a>
	</h1>
</body>
</html>