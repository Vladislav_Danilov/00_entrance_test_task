CREATE USER 'admin'@'localhost' IDENTIFIED BY 'password';
GRANT ALL PRIVILEGES ON mydb.mark_form TO 'admin'@'localhost';
GRANT ALL PRIVILEGES ON mydb.abiturient TO 'admin'@'localhost';
FLUSH PRIVILEGES;
CREATE USER 'admin'@'%' IDENTIFIED BY 'password';
GRANT ALL PRIVILEGES ON mydb.mark_form TO 'admin'@'%';
GRANT ALL PRIVILEGES ON mydb.abiturient TO 'admin'@'%';
FLUSH PRIVILEGES;
INSERT INTO `mydb`.`users_bd` (`name`, `status`) VALUES ('admin', 'admin');