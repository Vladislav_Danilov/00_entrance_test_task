/*запрос выводит всех поступивших студентов*/
SET @n := 0;
Select id, last_name, name, middle_name, result.name_f, AVG_mark_attestat, средняя_оценка from (
Select @n := @n+1 AS id, last_name, name, middle_name, name_f, AVG_mark_attestat, средняя_оценка from (
Select abiturient.last_name, abiturient.name, abiturient.middle_name, faculty.name_f, AVG_mark_attestat,
AVG(mark_form.mark) AS средняя_оценка
from admin_form, abiturient, faculty, subject, mark_form
where mark_form_idform=idform and idabiturient=mark_form_abiturient_idabiturient and mark_form_subject_faculty_idfaculty=idfaculty
and  name_f = 'Математический'
group by last_name 
order by средняя_оценка DESC, AVG_mark_attestat DESC) AS result) AS result, faculty
where id<= faculty.num_student
group by last_name
order by средняя_оценка DESC, AVG_mark_attestat DESC