/*����� ��������� ������ ������ � ������ �������������� �� ����� ��������*/
package prop;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

public class Properties {
	private static String login;
	private static String password;
	private static Logger log = Logger.getLogger(Properties.class);

	public static String getLogin() {
		return login;
	}

	public static void setLogin(String login) {
		Properties.login = login;
	}

	public static String getPassword() {
		return password;
	}

	public static void setPassword(String password) {
		Properties.password = password;
	}

	/*
	 * ����� ��������� ������ �� ����� properties � ���������� �� � ������, ��
	 * �������� ���������� ������ ������������� ��������
	 */
	public Properties() {
		log.info("start method");
		InputStream in = getClass().getClassLoader().getResourceAsStream(
				"prop.txt");
		BufferedReader br = new BufferedReader(new InputStreamReader(in));
		String line;
		List<String> lines = new ArrayList<String>();
		try {
			while ((line = br.readLine()) != null) {
				lines.add(line);
			}
		} catch (IOException e) {
			log.error(e);
		}
		log.info("stop method");
		login = lines.get(3);
		password = lines.get(5);
	}
}
