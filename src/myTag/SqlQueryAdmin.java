/*��� ��������� ������� �� ������ ��������� � ���������� �������*/
package myTag;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.servlet.jsp.*;
import javax.servlet.jsp.tagext.*;
import org.apache.log4j.Logger;
import dao.*;
import to.AbiturientTO;

@SuppressWarnings("serial")
public class SqlQueryAdmin extends BodyTagSupport {
	private static Logger log = Logger.getLogger(SqlQueryAdmin.class);
	private int num = 0;
	private int idform = 0;
	private int idsubject = 0;
	private int idfaculty = 0;
	private int idabitur = 0;

	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}

	public int getIdform() {
		return idform;
	}

	public void setIdform(int idform) {
		this.idform = idform;
	}

	public int getIdsubject() {
		return idsubject;
	}

	public void setIdsubject(int idsubject) {
		this.idsubject = idsubject;
	}

	public int getIdfaculty() {
		return idfaculty;
	}

	public void setIdfaculty(int idfaculty) {
		this.idfaculty = idfaculty;
	}

	public int getIdabitur() {
		return idabitur;
	}

	public void setIdabitur(int idabitur) {
		this.idabitur = idabitur;
	}

	public int doStartTag() throws JspTagException {
		log.info("start method");
		ArrayList<AbiturientTO> print = new ArrayList<AbiturientTO>();
        String message = "";
		try {
			/* ���� ������ */
			switch (num) {
			/* ������ �� ���������� ����������� � ��������� */
			case 1:
				boolean verificator;
				if ((idform != 0) || (idsubject != 0) || (idfaculty != 0)
						|| (idabitur != 0)) {
					verificator = new AdminFormDAO().addAbiturient(idform, idsubject, idfaculty, idabitur);
					if(verificator==true){
					message="���������� ������� ��������������� � ���������.";
					}
					else
					{
						message="����������� ������� ������!!!";
					}
				} else {
					message="��� ���� ����������� ��� ����������.";
				}
				break;
			/* ������ �� ����� ���� ������ � �������������������� ������������ */
			case 2:
				print = new AbiturientDAO().selectAbiturient();

				break;
			}

		} catch (SQLException e1) {
			log.error(e1);
		}
		try {
			pageContext.getOut().write(message);
		} catch (IOException e2) {
			log.error(e2);
		}
		/* ����� ������ �� ����� � ������� � ��� */
		/* ������� ������������� ������ ������� � ������ */
		if (print.size() > 1) {
			try {
				pageContext.getOut().write("<TABLE WIDTH=\"100%\">");
			} catch (IOException e2) {
				log.error(e2);
			}
			/*
			 * ���������� ����� ������� ���������� ������� ����������� �
			 * ����������� �������
			 */
			try {
			pageContext.getOut().write("<TR>");
			pageContext.getOut().write("<TD>" + "ID_�����������" + "</TD>");
			pageContext.getOut().write("<TD>" + "���" + "</TD>");
			pageContext.getOut().write("<TD>" + "�������" + "</TD>");
			pageContext.getOut().write("<TD>" + "��������" + "</TD>");
			pageContext.getOut().write("<TD>" + "���� ��������" + "</TD>");
			pageContext.getOut().write("<TD>" + "���� �����������" + "</TD>");
			pageContext.getOut().write("<TD>" + "������� ������ ��������" + "</TD>");
			pageContext.getOut().write("<TD>" + "ID ����� ������" + "</TD>");
			pageContext.getOut().write("<TD>" + "������" + "</TD>");
			pageContext.getOut().write("<TD>" + "ID ��������" + "</TD>");
			pageContext.getOut().write("<TD>" + "ID ����������" + "</TD>");
			pageContext.getOut().write("</TR>");
			} catch (IOException e1) {
				log.error(e1);
			}
			for (AbiturientTO item : print) {
				try {
				pageContext.getOut().write("<TR>");

					pageContext.getOut().write("<TD>" + item.getId_abitur() + "</TD>");
					pageContext.getOut().write("<TD>" + item.getName() + "</TD>");
					pageContext.getOut().write("<TD>" + item.getLast_name() + "</TD>");
					pageContext.getOut().write("<TD>" + item.getMiddle_name() + "</TD>");
					pageContext.getOut().write("<TD>" + item.getBirthdate() + "</TD>");
					pageContext.getOut().write("<TD>" + item.getDatereg() + "</TD>");
					pageContext.getOut().write("<TD>" + item.getMarkAttestat() + "</TD>");
					pageContext.getOut().write("<TD>" + item.getId_form() + "</TD>");
					pageContext.getOut().write("<TD>" + item.getMark() + "</TD>");
					pageContext.getOut().write("<TD>" + item.getId_subject() + "</TD>");
					pageContext.getOut().write("<TD>" + item.getId_faculty() + "</TD>");
				    pageContext.getOut().write("</TR>");
				} catch (IOException e) {
					log.error(e);
				}
			}
			try {
				pageContext.getOut().write("</TABLE>");
			} catch (IOException e) {
				log.error(e);
			}
		}
		log.info("stop method");
		return SKIP_BODY;
	}

}
