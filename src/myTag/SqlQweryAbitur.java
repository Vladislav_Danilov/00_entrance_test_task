/*��� ��������� ������� �� ������ ��������� � ���������� �������*/
package myTag;

import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.jsp.*;
import javax.servlet.jsp.tagext.*;
import org.apache.log4j.Logger;
import dao.*;
import to.FacultyTO;

@SuppressWarnings("serial")
public class SqlQweryAbitur extends BodyTagSupport {
	private static Logger log = Logger.getLogger(MarketDAO.class);
	private int num;
	private String name;
	private String last_name;
	private String middle_name;
	private String birthdate;
	private String datereg;
	private float markAttestat;
	private int mark;
	private String name_subject;
	private String name_faculty;
	private int mark2;
	private String name_subject2;

	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLast_name() {
		return last_name;
	}

	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}

	public String getMiddle_name() {
		return middle_name;
	}

	public void setMiddle_name(String middle_name) {
		this.middle_name = middle_name;
	}

	public String getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(String birthdate) {
		this.birthdate = birthdate;
	}

	public String getDatereg() {
		return datereg;
	}

	public void setDatereg(String datereg) {
		this.datereg = datereg;
	}

	public float getMarkAttestat() {
		return markAttestat;
	}

	public void setMarkAttestat(float markAttestat) {
		this.markAttestat = markAttestat;
	}

	public int getMark() {
		return mark;
	}

	public void setMark(int mark) {
		this.mark = mark;
	}

	public String getName_subject() {
		return name_subject;
	}

	public void setName_subject(String name_subject) {
		this.name_subject = name_subject;
	}

	public String getName_faculty() {
		return name_faculty;
	}

	public void setName_faculty(String name_faculty) {
		this.name_faculty = name_faculty;
	}

	public int getMark2() {
		return mark2;
	}

	public void setMark2(int mark2) {
		this.mark2 = mark2;
	}

	public String getName_subject2() {
		return name_subject2;
	}

	public void setName_subject2(String name_subject2) {
		this.name_subject2 = name_subject2;
	}


	public int doStartTag() throws JspTagException {
		log.info("start method");
		ArrayList<FacultyTO> print = new ArrayList<FacultyTO>();
		String message = "";
		/* ���� ������ */
		switch (num) {
		/* ������ �� ���������� ����������� */
		case 1:
			boolean verificator;
			if ((last_name != "") || (name != "") || (middle_name != "")) {
				verificator = new AbiturientDAO().addAbiturient(name, last_name, middle_name,
						birthdate, datereg, markAttestat, mark, name_subject,
						name_faculty, mark2, name_subject2);
				if(verificator == true){
				message = "�����������!!! ";
				message = "�� ����������������� � ����� �������. ������� �����������.";
				}
				else
				{
					message="����������� ������� ������!!!";
				}
			}
			break;
		/* ������ �� ����� ����������� � ��� */
		case 2:
			if ((name != "") && (name != " ")) {
				print = new ResultDAO().resultOutput(name);
			}

			break;
		}
		try {
			pageContext.getOut().write(message);
		} catch (IOException e2) {
			log.error(e2);
		}
		/* ����� ������ �� ����� � ������� � ��� */
		/* ������� ������������� ������ ������� � ������ */
		if (print.size() > 1) {
			try {
				pageContext.getOut().write("<TABLE WIDTH=\"100%\">");
			} catch (IOException e2) {
				log.error(e2);
			}
			/*
			 * print.add(
			 * "ID ������� ��� �������� ��������_���������� �������_������_��������"
			 * );
			 */
			try {
				pageContext.getOut().write("<TR>");
				pageContext.getOut().write("<TD>" + "ID" + "</TD>");
				pageContext.getOut().write("<TD>" + "�������" + "</TD>");
				pageContext.getOut().write("<TD>" + "���" + "</TD>");
				pageContext.getOut().write("<TD>" + "��������" + "</TD>");
				pageContext.getOut().write(
						"<TD>" + "�������� ����������" + "</TD>");
				pageContext.getOut().write(
						"<TD>" + "������� ������ ��������" + "</TD>");
				pageContext.getOut().write("</TR>");
			} catch (IOException e1) {
				log.error(e1);
			}
			/*
			 * ���������� ����� ������� ���������� ������� ����������� �
			 * ����������� �������
			 */
			for (FacultyTO item : print) {
				try {
					pageContext.getOut().write("<TR>");
					pageContext.getOut().write("<TD>" + item.getId() + "</TD>");
					pageContext.getOut().write(
							"<TD>" + item.getMiddle_name_s() + "</TD>");
					pageContext.getOut().write(
							"<TD>" + item.getName_s() + "</TD>");
					pageContext.getOut().write(
							"<TD>" + item.getLast_name_s() + "</TD>");
					pageContext.getOut().write(
							"<TD>" + item.getName_f() + "</TD>");
					pageContext.getOut().write(
							"<TD>" + item.getMark_at() + "</TD>");
					pageContext.getOut().write("</TR>");
				} catch (IOException e) {
					log.error(e);
				}
			}
			try {
				pageContext.getOut().write("</TABLE>");
			} catch (IOException e) {
				log.error(e);
			}
		}
		log.info("stop method");
		return SKIP_BODY;
	}

}
