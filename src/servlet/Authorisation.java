/*������� ��������� ����������� ������������ � ������� �������� ��������*/
package servlet;

import dao.*;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import prop.PropertiesSession;
import to.AutorisationTO;

public class Authorisation extends HttpServlet {
	private static Logger log = Logger.getLogger(Authorisation.class);
	private static final long serialVersionUID = 1L;

	public Authorisation() {
		super();
	}

	@SuppressWarnings("static-access")
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		log.info("start method");

		if (request.getParameter("command").equals("forward")) {
			/* ���������� ���������� ������ */
			HttpSession session = request.getSession(true);
			String login = new String(request.getParameter("login").getBytes(
					"ISO-8859-1"), "UTF-8");
			AutorisationTO status = new AutorisationDAO().Status(login);
			/* ������� ���� � ����� */
			DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			Date date = new Date();
			String dates = dateFormat.format(date);
			/* ip adress */
			InetAddress addr = InetAddress.getLocalHost();
			String myIP = addr.getHostAddress();
			String title = "������!";
			response.setContentType("text/html;charset=utf-8");
			response.setCharacterEncoding("UTF-8");
			request.setCharacterEncoding("utf-8");
			PrintWriter out = new PrintWriter(new OutputStreamWriter(
					response.getOutputStream(), "UTF8"), true);
			out.println("<HTML><HEAD><link href='style.css' rel='stylesheet'><TITLE>");
			out.println(title);
			out.println("</TITLE></HEAD><BODY>");
			out.println("<br>������� �����: " + dates + "</br>");
			out.println("������� IP: " + myIP);

			if (status.getStatus().equals("abitur") == true) {
				out.println("<h1>������ ����������!!!</h1>");
				out.println("<br>��� ���������� ����������� ������� �� ��� ��������</br>");
				out.println("<br><a href='abiturient.jsp'>" + "�����������"
						+ "</a></br>");
				out.println("<br><a href='index.jsp'>" + "�����" + "</a></br>");
			}

			if (status.getStatus().equals("admin") == true) {
				out.println("<h1>������������ �������������!!!</h1>");
				out.println("<br>��� ���������� ������ ��������� �� ��� ��������</br>");
				out.println("<br><a href='administrator.jsp'>"
						+ "���������� ���������" + "</a></br>");
				out.println("<br><a href='index.jsp'>" + "�����" + "</a></br>");
			}

			if ((status.getStatus().equals("abitur") == false)
					&& (status.getStatus().equals("admin") == false)) {
				out.println("<h1>������������ �����������!!!</h1>");
				out.println("<br>��� ��� � ������ �������������. ����������������� "
						+ "� �������������� ���� ������</br>");
				out.println("<br><a href='index.jsp'>" + "�����" + "</a></br>");
			}
			out.println("</BODY></HTML>");
			out.close();

			/*
			 * �������� ������ ��� �������� ������ ����������� ��� ����������
			 * ������
			 */
			session.setAttribute("Login", request.getParameter("login"));
			session.setAttribute("Password", request.getParameter("password"));
			PropertiesSession connectProp = new PropertiesSession();
			connectProp.setLogin((String) request.getSession().getAttribute("Login")); 
			connectProp.setPassword((String) request.getSession().getAttribute("Password"));
		}
		log.info("stop method");
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
	}

}
