/*������� ������������ ������� ���������� ��������� ������������ � ����� ��������*/
package servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import prop.PropertiesSession;


public class AbiturInput extends HttpServlet {
	private static Logger log = Logger.getLogger(AbiturInput.class);
	private static final long serialVersionUID = 1L;

	public AbiturInput() {
		super();
	}

	@SuppressWarnings("static-access")
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		log.info("start method");
		String name = "";
		String last_name = "";
		String middle_name = "";
		String birthdate = "";
		String datereg = "";
		String markAttestat = "3";
		String name_faculty = "";
		String name_subject = "";
		String mark = "3";
		String name_subject2 = "";
		String mark2 = "3";
		/* ������� �������� � �������� ��������� */
		name = new String(request.getParameter("name").getBytes("ISO-8859-1"),
				"UTF-8");
		request.setAttribute("name", name);
		last_name = new String(request.getParameter("last_name").getBytes(
				"ISO-8859-1"), "UTF-8");
		request.setAttribute("last_name", last_name);
		middle_name = new String(request.getParameter("middle_name").getBytes(
				"ISO-8859-1"), "UTF-8");
		request.setAttribute("middle_name", middle_name);
		birthdate = new String(request.getParameter("birthdate").getBytes(
				"ISO-8859-1"), "UTF-8");
		request.setAttribute("birthdate", birthdate);
		datereg = new String(request.getParameter("datereg").getBytes(
				"ISO-8859-1"), "UTF-8");
		request.setAttribute("datereg", datereg);
		markAttestat = new String(request.getParameter("markAttestat")
				.getBytes("ISO-8859-1"), "UTF-8");
		request.setAttribute("markAttestat", markAttestat);
		name_faculty = new String(request.getParameter("name_faculty")
				.getBytes("ISO-8859-1"), "UTF-8");
		request.setAttribute("name_faculty", name_faculty);
		name_subject = new String(request.getParameter("name_subject")
				.getBytes("ISO-8859-1"), "UTF-8");
		request.setAttribute("name_subject", name_subject);
		mark = new String(request.getParameter("mark").getBytes("ISO-8859-1"),
				"UTF-8");
		request.setAttribute("mark", mark);
		name_subject2 = new String(request.getParameter("name_subject2")
				.getBytes("ISO-8859-1"), "UTF-8");
		request.setAttribute("name_subject2", name_subject2);
		mark2 = new String(
				request.getParameter("mark2").getBytes("ISO-8859-1"), "UTF-8");
		request.setAttribute("mark2", mark2);

		/*
		 * ����� ���������� ������ autorisation ���������� �������� ������ �
		 * ������
		 */
		PropertiesSession setting = new PropertiesSession();
		if ((setting.getLogin() == null) && (setting.getPassword() == null)) {
			request.getRequestDispatcher("index.jsp")
					.forward(request, response);
		} else {
			request.getRequestDispatcher("abiturient.jsp").forward(request,
					response);
		}
		log.info("stop method");
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
	}

}
