/*������� �������� �������� ���� ����� ���������� � ���������� � ��������*/
package servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import prop.PropertiesSession;

public class ResultOut extends HttpServlet {
	private static Logger log = Logger.getLogger(ResultOut.class);
	private static final long serialVersionUID = 1L;

	public ResultOut() {
		super();

	}

	@SuppressWarnings("static-access")
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		log.info("start method");
		String name = "";

		/* ������� �������� � �������� ��������� */
		name = new String(request.getParameter("name").getBytes("ISO-8859-1"),
				"UTF-8");
		request.setAttribute("list", name);
		/*
		 * ����� ���������� ������ autorisation ���������� �������� ������ �
		 * ������
		 */
		
		PropertiesSession setting = new PropertiesSession();
		if ((setting.getLogin() == null) && (setting.getPassword() == null)) {
			request.getRequestDispatcher("index.jsp")
					.forward(request, response);
		} else {
			request.getRequestDispatcher("abiturient.jsp").forward(request,
					response);
		}
		log.info("stop method");
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

	}

}
