/*����� ��������� ����������� � ������ � ������� ������ � ������� admin_form*/
package dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.log4j.Logger;

import prop.PropertiesSession;

interface IAdminFormDAO {
	public boolean addAbiturient(int idform, int idsubject, int idfaculty, int idabitur);
}

/* ����� ��������� ������ �� ���������� ����������� � ��������� */
public class AdminFormDAO extends MarketDAO implements IAdminFormDAO {
	private static Logger log = Logger.getLogger(AdminFormDAO.class);
	@SuppressWarnings("static-access")
	public boolean addAbiturient(int idform, int idsubject, int idfaculty, int idabitur) {
		log.info("start method");
		Connection cn = null;
		Statement st = null;
		PropertiesSession connectProp = new PropertiesSession();
		try {
			cn = MarketDAO.getConn(connectProp.getLogin(), connectProp.getPassword());
		} catch (SQLException e) {
			log.error(e);
		}
		try {
			st = cn.createStatement();
		} catch (SQLException e2) {
		  log.error(e2);
		}
		try {
			st
					.executeUpdate(
							"INSERT INTO admin_form "
									+ "(mark_form_idform, mark_form_subject_idsubject, mark_form_subject_faculty_idfaculty"
									+ ",mark_form_abiturient_idabiturient)"
									+ " VALUES (" + idform + ", "
									+ idsubject + ", "
									+ idfaculty + ", "
									+ idabitur + ");");
		} catch (SQLException e) {
			log.error(e);
			return false;
		}
		try {
			st.close();
		} catch (SQLException e1) {
			log.error(e1);
		}
		try {
			cn.close();
		} catch (SQLException e) {
			log.error(e);
		}
		log.info("stop method");
		return true;
	}
}
