/*����� ��������� ����� ������ � ����������� ������������*/
package dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import prop.PropertiesSession;
import to.FacultyTO;

interface IResultDAO {
	public ArrayList<FacultyTO> resultOutput(String faculty_name);
}

public class ResultDAO extends MarketDAO implements IResultDAO {
	private static Logger log = Logger.getLogger(ResultDAO.class);
	/* ����� ������ ������ �� ������ ����������� ������������ */
	@SuppressWarnings("static-access")
	public ArrayList<FacultyTO> resultOutput(String faculty_name) {
		PropertiesSession connectProp=new PropertiesSession();
		log.info("start method");
		FacultyTO faculty = new FacultyTO(0, faculty_name, faculty_name, faculty_name, faculty_name, 0);
		faculty.setName_f(faculty_name);
		Connection cn = null;
		Statement st = null;
		try {
			cn = MarketDAO.getConn(connectProp.getLogin(), connectProp.getPassword());
		} catch (SQLException e2) {
			log.error(e2);
		}
		try {
			st = cn.createStatement();
		} catch (SQLException e2) {
		  log.error(e2);
		}
		ResultSet rs = null;
		try {
			rs = st.executeQuery("SET @n := 0;");
		} catch (SQLException e) {
			log.error(e);
		}
		try {
			rs = st
					.executeQuery(
							"Select id, last_name, name, middle_name, result.name_f, AVG_mark_attestat, �������_������ from (Select @n := @n+1 AS id, last_name, name, middle_name, name_f, AVG_mark_attestat, �������_������ from (Select abiturient.last_name, abiturient.name, abiturient.middle_name, faculty.name_f, AVG_mark_attestat,AVG(mark_form.mark) AS �������_������ from admin_form, abiturient, faculty, subject, mark_form where mark_form_idform=idform and idabiturient=mark_form_abiturient_idabiturient and mark_form_subject_faculty_idfaculty=idfaculty and  name_f = '"
									+ faculty.getName_f()
									+ "' group by last_name order by �������_������ DESC, AVG_mark_attestat DESC) AS result) AS result, faculty where id<= faculty.num_student group by last_name order by �������_������ DESC, AVG_mark_attestat DESC");
		} catch (SQLException e) {
			log.error(e);
		}
		ArrayList<FacultyTO> print = new ArrayList<FacultyTO>();
		try {
			
			while (rs.next()) {

				try {
					print.add(new FacultyTO(rs.getInt(1), rs.getString(2),
							rs.getString(3), rs.getString(4),
							rs.getString(5), rs.getFloat(7)));
				} catch (SQLException e1) {
					log.error(e1);
				}
			}
		} catch (SQLException e1) {
			log.error(e1);
		}
		try {
			rs.close();
		} catch (SQLException e) {
			log.error(e);
		}
		try {
			st.close();
		} catch (SQLException e1) {
			log.error(e1);
		}
		try {
			cn.close();
		} catch (SQLException e) {
			log.error(e);
		}
		log.info("stop method");
		return print;
	}
}
