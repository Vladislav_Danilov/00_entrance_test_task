/*����� ��������� ����������� � ���� ������ ����� ���������� conection2015*/
package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import org.apache.log4j.Logger;

public abstract class MarketDAO {
	private static Logger log = Logger.getLogger(MarketDAO.class);
	public static Connection conn = null;

	public static Connection getConn(String user, String password)
			throws SQLException {
		log.info("start method");
			try {
				initConn(user, password);
			} catch (ClassNotFoundException e) {
				log.error(e);
			}
			log.info("stop method");
			return conn;
	}

	protected static void initConn(String user, String password)
			throws SQLException, ClassNotFoundException {
		log.info("start method");
		Class.forName("org.gjt.mm.mysql.Driver");
		Properties p = new Properties();
		p.setProperty("user", user);
		p.setProperty("password", password);
		/*
		 * ��� ������ ����� ��������� ������ ��������� ��������, ��� �����
		 * ����������� ������ � �������� �������
		 */
		p.setProperty("useUnicode", "true");
		p.setProperty("characterEncoding", "utf-8");
		conn = DriverManager.getConnection("jdbc:mysql://127.0.0.1/mydb", p);
		log.info("stop method");
	}

	public static void closeConn() throws SQLException {
				conn.close();
	}
}
