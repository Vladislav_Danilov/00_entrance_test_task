/*����� ��������� ����������� � ������ � ������� ������ � ������� abiturient, mark_form*/
package dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import to.*;

import org.apache.log4j.*;

import prop.PropertiesSession;

interface IAbiturientDAO {
	public boolean addAbiturient(String name, String last_name,
			String middle_name, String birthdate, String datereg,
			float markAttestat, int mark, String name_subject,
			String name_faculty, int mark2, String name_subject2);

	public ArrayList <AbiturientTO> selectAbiturient() throws SQLException;
}

/* ����������� ������ ����������� */
public class AbiturientDAO extends MarketDAO implements IAbiturientDAO {
	private static Logger log = Logger.getLogger(AbiturientDAO.class);
	/* ������ ��������� ������ ����������� */
	@SuppressWarnings("static-access")
	public boolean addAbiturient(String name,
			String last_name, String middle_name, String birthdate,
			String datereg, float markAttestat, int mark, String name_subject,
			String name_faculty, int mark2, String name_subject2) {
		log.info("start method");
		AbiturientTO abiturient = new AbiturientTO(mark2, name_subject2,
				name_subject2, name_subject2, name_subject2, name_subject2,
				mark2, mark2, mark2, mark2, mark2);
		abiturient.setName(name);
		abiturient.setLast_name(last_name);
		abiturient.setMiddle_name(middle_name);
		abiturient.setBirthdate(birthdate);
		abiturient.setDatereg(datereg);
		abiturient.setMarkAttestat(markAttestat);
		FacultyTO faculty = new FacultyTO(mark2, name_faculty, name_subject2, name_subject2, name_subject2, mark2);
		faculty.setName_f(name_faculty);
		Connection cn = null;
		Statement st = null;
		PropertiesSession connectProp = new PropertiesSession();
		try {
			cn = MarketDAO.getConn(connectProp.getLogin(), connectProp.getPassword());
		} catch (SQLException e1) {
			log.error(e1);
		}
		try {
			st = cn.createStatement();
		} catch (SQLException e2) {
		  log.error(e2);
		}
		try {
			st.executeUpdate(
							"INSERT INTO abiturient (name, last_name, middle_name, birthdate, datereg, AVG_mark_attestat) "
									+ " VALUES ('"
									+ abiturient.getName()
									+ "', '"
									+ abiturient.getLast_name()
									+ "', "
									+ "'"
									+ abiturient.getMiddle_name()
									+ "', '"
									+ abiturient.getBirthdate()
									+ "', "
									+ "'"
									+ abiturient.getDatereg()
									+ "', "
									+ abiturient.getMarkAttestat() + ");");
		} catch (SQLException e) {
			log.error(e);
			return false;
		}
		try {
			st
					.executeUpdate(
							" INSERT INTO mark_form (mark, subject_idsubject, subject_faculty_idfaculty, abiturient_idabiturient)"
									+ " SELECT "
									+ mark
									+ ", idsubject, idfaculty, idabiturient from faculty, abiturient, subject"
									+ " WHERE subject.name='"
									+ name_subject
									+ "' "
									+ "and faculty.name_f='"
									+ faculty.getName_f()
									+ "' "
									+ "and abiturient.name='"
									+ abiturient.getName()
									+ "'"
									+ " and abiturient.last_name='"
									+ abiturient.getLast_name()
									+ "' "
									+ "and abiturient.middle_name='"
									+ abiturient.getMiddle_name() + "';");
		} catch (SQLException e) {
			log.error(e);
			return false;
		}
		try {
			st
					.executeUpdate(
							" INSERT INTO mark_form (mark, subject_idsubject, subject_faculty_idfaculty, abiturient_idabiturient)"
									+ " SELECT "
									+ mark2
									+ ", idsubject, idfaculty, idabiturient from faculty, abiturient, subject"
									+ " WHERE subject.name='"
									+ name_subject2
									+ "' "
									+ "and faculty.name_f='"
									+ faculty.getName_f()
									+ "' "
									+ "and abiturient.name='"
									+ abiturient.getName()
									+ "'"
									+ " and abiturient.last_name='"
									+ abiturient.getLast_name()
									+ "' "
									+ "and abiturient.middle_name='"
									+ abiturient.getMiddle_name() + "';");
		} catch (SQLException e) {
			log.error(e);
			return false;
		}
		try {
			st.close();
		} catch (SQLException e1) {
			log.error(e1);
		}
		try {
			cn.close();
		} catch (SQLException e) {
			log.error(e);
		}
		log.info("stop method");
		return true;
	}

	/*
	 * ������ ������� ������ ���������� � ���� ������������������ ������������
	 * ��� ����������� ����� ������ � ���������
	 */
	@SuppressWarnings("static-access")
	public ArrayList <AbiturientTO> selectAbiturient()
			throws SQLException {
		log.info("start method");
		Connection cn = null;
		Statement st = null;
		PropertiesSession connectProp = new PropertiesSession();
		cn = MarketDAO.getConn(connectProp.getLogin(), connectProp.getPassword());
		try {
			st = cn.createStatement();
		} catch (SQLException e2) {
		  log.error(e2);
		}
		ResultSet rs = null;
		try {
			rs = st.executeQuery(
					"Select * from abiturient, mark_form "
							+ "where idabiturient = abiturient_idabiturient");
		} catch (SQLException e1) {
			log.error(e1);
		}
		ArrayList <AbiturientTO> print = new ArrayList<AbiturientTO>();
		try {
			while (rs.next()) {

				try {
					print.add(new AbiturientTO(rs.getInt(1), rs.getString(2),
							rs.getString(3), rs.getString(4),
							rs.getString(5),rs.getString(6),
							rs.getInt(7), rs.getInt(8),
							rs.getInt(9), rs.getInt(10),
							rs.getInt(11)));
				} catch (SQLException e1) {
					log.error(e1);
				}
			}
		} catch (SQLException e1) {
			log.error(e1);
		}
		try {
			rs.close();
		} catch (SQLException e) {
			log.error(e);
		}
		try {
			st.close();
		} catch (SQLException e1) {
			log.error(e1);
		}
		try {
			cn.close();
		} catch (SQLException e) {
			log.error(e);
		}
		log.info("stop method");
		return print;
	}
}
