/*����� ���� DAO, �������� �� �������� ������� ������������ ��� �������� Autorisation*/
package dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import org.apache.log4j.Logger;
import prop.Properties;
import to.*;

interface IAutorisationDAO {
	public AutorisationTO Status(String login);
}
public class AutorisationDAO extends MarketDAO implements IAutorisationDAO {
	private static Logger log = Logger.getLogger(AutorisationDAO .class);
	/*����� ������ ������ � ������� ���������� ������� ������������� � ���������� ���������� ���������� ���������*/
	@SuppressWarnings("static-access")
	public AutorisationTO Status(String login) {
		log.info("start method");
		AutorisationTO status = new AutorisationTO("");
		Properties prop = new Properties();
		Connection cn = null;
		Statement st = null;
		ResultSet rs = null;
		try {

			cn = MarketDAO.getConn(prop.getLogin(), prop.getPassword());
			try {
				st = cn.createStatement();
			} catch (SQLException e2) {
			  log.error(e2);
			}
			

			/* ������ ������������ � �� */
			rs = st.executeQuery(
					"SELECT status FROM mydb.users_bd where name='"
							+ login + "'");
			while (rs.next()) {

				try {
					/* ���������� ����� ������� ������ ������������ */
					status = new AutorisationTO(rs.getString(1));
				} catch (SQLException e1) {
					log.error(e1);
				}
			}
		} catch (SQLException e) {
			log.error(e);
		}
		try {
			rs.close();
		} catch (SQLException e) {
			log.error(e);
		}
		try {
			st.close();
		} catch (SQLException e1) {
			log.error(e1);
		}
		try {
			cn.close();
		} catch (SQLException e) {
			log.error(e);
		}
		log.info("stop method");
		return status;
		
	}

}
