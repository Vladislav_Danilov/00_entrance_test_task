/*����� ��������� ������ ���� DAO ��������������� � ��������� Registration*/
package dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.log4j.Logger;

import prop.Properties;

interface IRegistrationDAO {
  public boolean Registrat(String login, String password);
}

public class RegistrationDAO extends MarketDAO implements IRegistrationDAO{
	private static Logger log = Logger.getLogger(RegistrationDAO.class);
	@SuppressWarnings("static-access")
	/*����� ������������ ������������ � ���� ������ � � ������� �������������*/
	public boolean Registrat(String login, String password) {
		log.info("start method");
		Properties prop = new Properties();
		Connection cn = null;
		Statement st = null;
		if((login!="")||(password!="")){
			try {
				cn = MarketDAO.getConn(prop.getLogin(), prop.getPassword());
			} catch (SQLException e2) {
				log.error(e2);
			}
			try {
				st = cn.createStatement();
			} catch (SQLException e2) {
			  log.error(e2);
			}
			try {
				/*������ �� ���������� ������������*/
				st.executeUpdate("CREATE USER '"+login+"'@'localhost' IDENTIFIED BY '"+password+"';");
				st.executeUpdate("GRANT INSERT, SELECT ON mydb.mark_form TO '"+login+"'@'localhost';");
				st.executeUpdate("GRANT INSERT, SELECT ON mydb.abiturient TO '"+login+"'@'localhost';");
				st.executeUpdate("GRANT SELECT ON mydb.admin_form TO '"+login+"'@'localhost';");
				st.executeUpdate("GRANT INSERT, SELECT ON mydb.faculty TO '"+login+"'@'localhost';");
				st.executeUpdate("GRANT INSERT, SELECT  ON mydb.subject TO '"+login+"'@'localhost';");
				st.executeUpdate("FLUSH PRIVILEGES;");
				st.executeUpdate("CREATE USER '"+login+"'@'%' IDENTIFIED BY '"+password+"';");
				st.executeUpdate("GRANT INSERT, SELECT  ON mydb.mark_form TO '"+login+"'@'%';");
				st.executeUpdate("GRANT INSERT, SELECT ON mydb.abiturient TO '"+login+"'@'%';");
				st.executeUpdate("GRANT SELECT ON mydb.admin_form TO '"+login+"'@'%';");
				st.executeUpdate("GRANT INSERT, SELECT ON mydb.faculty TO '"+login+"'@'%';");
				st.executeUpdate("GRANT INSERT, SELECT ON mydb.subject TO '"+login+"'@'%';");
				st.executeUpdate("FLUSH PRIVILEGES;");
				st.executeUpdate("INSERT INTO `mydb`.`users_bd` (`name`, `status`) VALUES ('" + login +"', 'abitur');");
			} catch (SQLException e) {
				log.error(e);
			}
			}
		try {
			st.close();
		} catch (SQLException e1) {
			log.error(e1);
		}
		try {
			cn.close();
		} catch (SQLException e) {
			log.error(e);
		}
		log.info("stop method");
		return true;
	}
	
}
