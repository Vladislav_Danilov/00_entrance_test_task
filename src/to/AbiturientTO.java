/*����� ��������� ���� ������ ������� ����� ����� DAO � ������� ������, �������� ������ �����������,
 * ������ � ������ ���������� ��� ���������� ��������� ���������������
 */
package to;

public class AbiturientTO {
	private int id_abitur;
	private String name;
	private String last_name;
	private String middle_name;
	private String birthdate;
	private String datereg;
	private float markAttestat;
	private int id_form;
	private int mark;
	private int id_subject;
	private int id_faculty;

	public AbiturientTO(int id_abitur, String name, String last_name,
			String middle_name, String birthdate, String datereg,
			int markAttestat, int id_form, int mark, int id_subject,
			int id_faculty) {
		super();
		this.id_abitur = id_abitur;
		this.name = name;
		this.last_name = last_name;
		this.middle_name = middle_name;
		this.birthdate = birthdate;
		this.datereg = datereg;
		this.markAttestat = markAttestat;
		this.id_form = id_form;
		this.mark = mark;
		this.id_subject = id_subject;
		this.id_faculty = id_faculty;
	}



	public String getBirthdate() {
		return birthdate;
	}



	public void setBirthdate(String birthdate) {
		this.birthdate = birthdate;
	}



	public String getDatereg() {
		return datereg;
	}



	public void setDatereg(String datereg) {
		this.datereg = datereg;
	}



	public int getId_form() {
		return id_form;
	}



	public void setId_form(int id_form) {
		this.id_form = id_form;
	}



	public int getMark() {
		return mark;
	}



	public void setMark(int mark) {
		this.mark = mark;
	}



	public int getId_subject() {
		return id_subject;
	}



	public void setId_subject(int id_subject) {
		this.id_subject = id_subject;
	}



	public int getId_faculty() {
		return id_faculty;
	}



	public void setId_faculty(int id_faculty) {
		this.id_faculty = id_faculty;
	}



	public int getId_abitur() {
		return id_abitur;
	}

	public void setId_abitur(int id_abitur) {
		this.id_abitur = id_abitur;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLast_name() {
		return last_name;
	}

	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}

	public String getMiddle_name() {
		return middle_name;
	}

	public void setMiddle_name(String middle_name) {
		this.middle_name = middle_name;
	}



	public float getMarkAttestat() {
		return markAttestat;
	}



	public void setMarkAttestat(float markAttestat) {
		this.markAttestat = markAttestat;
	}

	
}
