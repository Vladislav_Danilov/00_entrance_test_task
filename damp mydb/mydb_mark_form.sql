-- MySQL dump 10.13  Distrib 5.6.23, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: mydb
-- ------------------------------------------------------
-- Server version	5.6.24

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `mark_form`
--

DROP TABLE IF EXISTS `mark_form`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mark_form` (
  `idform` int(11) NOT NULL AUTO_INCREMENT,
  `mark` int(11) DEFAULT NULL,
  `subject_idsubject` int(11) NOT NULL,
  `subject_faculty_idfaculty` int(11) NOT NULL,
  `abiturient_idabiturient` int(11) NOT NULL,
  PRIMARY KEY (`idform`,`subject_idsubject`,`subject_faculty_idfaculty`,`abiturient_idabiturient`),
  KEY `fk_form_subject1_idx` (`subject_idsubject`,`subject_faculty_idfaculty`),
  KEY `fk_form_abiturient1_idx` (`abiturient_idabiturient`),
  CONSTRAINT `fk_form_abiturient1` FOREIGN KEY (`abiturient_idabiturient`) REFERENCES `abiturient` (`idabiturient`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_form_subject1` FOREIGN KEY (`subject_idsubject`, `subject_faculty_idfaculty`) REFERENCES `subject` (`idsubject`, `faculty_idfaculty`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mark_form`
--

LOCK TABLES `mark_form` WRITE;
/*!40000 ALTER TABLE `mark_form` DISABLE KEYS */;
INSERT INTO `mark_form` VALUES (21,3,1,2,1),(22,4,2,2,1),(23,5,1,2,2),(24,5,2,2,2),(25,4,1,2,3),(26,3,2,2,3),(27,4,1,2,4),(28,5,2,2,4),(29,3,1,2,5),(30,4,2,2,5),(31,5,3,1,6),(32,4,4,1,6),(33,3,3,1,7),(34,4,4,1,7),(35,5,3,1,8),(36,4,4,1,8),(37,3,3,1,9),(38,4,4,1,9),(39,5,3,1,10),(40,4,4,1,10),(42,5,3,1,12),(43,5,4,1,12),(48,5,1,2,19),(49,5,2,2,19);
/*!40000 ALTER TABLE `mark_form` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-06-14 12:13:18
