-- MySQL dump 10.13  Distrib 5.6.23, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: mydb
-- ------------------------------------------------------
-- Server version	5.6.24

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin_form`
--

DROP TABLE IF EXISTS `admin_form`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_form` (
  `idadmin_form` int(11) NOT NULL AUTO_INCREMENT,
  `mark_form_idform` int(11) NOT NULL,
  `mark_form_subject_idsubject` int(11) NOT NULL,
  `mark_form_subject_faculty_idfaculty` int(11) NOT NULL,
  `mark_form_abiturient_idabiturient` int(11) NOT NULL,
  PRIMARY KEY (`idadmin_form`,`mark_form_idform`,`mark_form_subject_idsubject`,`mark_form_subject_faculty_idfaculty`,`mark_form_abiturient_idabiturient`),
  KEY `fk_admin_form_mark_form1_idx` (`mark_form_idform`,`mark_form_subject_idsubject`,`mark_form_subject_faculty_idfaculty`,`mark_form_abiturient_idabiturient`),
  CONSTRAINT `fk_admin_form_mark_form1` FOREIGN KEY (`mark_form_idform`, `mark_form_subject_idsubject`, `mark_form_subject_faculty_idfaculty`, `mark_form_abiturient_idabiturient`) REFERENCES `mark_form` (`idform`, `subject_idsubject`, `subject_faculty_idfaculty`, `abiturient_idabiturient`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_form`
--

LOCK TABLES `admin_form` WRITE;
/*!40000 ALTER TABLE `admin_form` DISABLE KEYS */;
INSERT INTO `admin_form` VALUES (1,21,1,2,1),(2,22,2,2,1),(3,23,1,2,2),(4,24,2,2,2),(5,25,1,2,3),(6,26,2,2,3),(7,27,1,2,4),(8,28,2,2,4),(9,29,1,2,5),(10,30,2,2,5),(11,31,3,1,6),(12,32,4,1,6),(13,33,3,1,7),(14,34,4,1,7),(15,35,3,1,8),(16,36,4,1,8),(17,37,3,1,9),(18,38,4,1,9),(19,39,3,1,10),(20,40,4,1,10),(24,43,4,1,12),(25,48,1,2,19),(27,49,2,2,19);
/*!40000 ALTER TABLE `admin_form` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-06-14 12:13:17
